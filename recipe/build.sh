#!/bin/bash

# Build the EPICS module
# Override PROJECT and LIBVERSION
# - PROJECT can't be guessed from the working directory
# - If we apply patches, the version will be set to the username
make PROJECT=asyn LIBVERSION=4.31.0
make PROJECT=asyn LIBVERSION=4.31.0 install
